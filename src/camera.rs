use std::io;
use v4l::video::capture::Parameters;
use v4l::prelude::*;
use v4l::FourCC;
use v4l::video::Capture;

pub fn get_camera(width: usize, height: usize) -> io::Result<Device> {
    // 0 for default camera
    let mut cam = Device::new(0)?;

    let mut format = cam.format()?;
    format.width = width as u32;
    format.height = height as u32;
    format.fourcc = FourCC::new(b"YUYV");
    cam.set_format(&format)?;

    // Highest supported by my camera
    cam.set_params(&Parameters::with_fps(10))?;

    Ok(cam)
}

// pub fn get_camera_opencv() -> opencv::Result<VideoCapture> {
//     // 0 for default camera
//     let cam = VideoCapture::new(0, CAP_ANY)?;
//     if !VideoCapture::is_opened(&cam)? {
//         // TODO: Make custom error type
//         return Err(opencv::Error::new(
//             -9999,
//             "Unable to open default camera!".to_owned(),
//         ));
//     }
//
//     Ok(cam)
// }
