#[allow(clippy::nursery)]
mod camera;
mod comms;
mod filter;
mod frame;
mod get_frames;
mod gui;
mod image_utils;
mod process_frames;
mod processing_thread;
mod v4l2loopback;

// use crate::camera::get_camera_opencv;
use crate::comms::Command;
// use crate::get_frames::record_frames;
use crate::gui::MainWindow;
use getopts::Options;
use std::env;
use std::error::Error;
use std::sync::mpsc;

// Good bitrate: 19.11 MB/s
// Base bitrate: 8.21 MB/s

const WIDTH: usize = 1280;
const HEIGHT: usize = 720;

fn main() -> Result<(), Box<dyn Error>> {
    let args: Vec<String> = env::args().collect();
    let mut opts = Options::new();
    opts.optopt("m", "mode", "Mode to run in, either \"train\" to take training footage or \"run\" to run the background subtractor.", "MODE");
    let matches = opts.parse(&args[1..])?;

    let mode = matches.opt_str("mode").unwrap();
    if &mode == "train" {
        // let mut cam = get_camera_opencv()?;
        // record_frames(&mut cam, 100, "bg_images/img_%02d.png")?;
    } else {
        run()?;
    }

    Ok(())
}

fn run() -> Result<(), Box<dyn Error>> {
    let (main_tx, thread_rx) = mpsc::channel();
    let (thread_tx, main_rx) = mpsc::channel();

    let processing_thread_handle = processing_thread::start(thread_rx, thread_tx, &main_rx)?;

    // GUI
    gtk::init().expect("Could not start GTK3");
    let window = MainWindow::new();
    window.start(main_tx.clone(), &main_rx);

    main_tx.send(Command::Stop)?;

    // TODO: Don't close window until this happens
    // Let the thread stop
    processing_thread_handle.join().unwrap();

    Ok(())
}
