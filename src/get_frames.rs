use std::io;
use v4l::io::traits::{Stream, CaptureStream};
use v4l::prelude::*;
use crate::frame::Frame;
use std::error::Error;

// TODO: Better error handling?
pub fn get_frame<'a>(
    stream: &'a mut dyn CaptureStream<'a, Item = [u8]>,
    width: usize,
    height: usize,
) -> Result<Frame, Box<dyn Error>> {
    // TODO: What is the metadata here?
    Ok(Frame::new(stream.next()?.0, width, height)?)
}

// pub fn get_frame_opencv(capture: &mut VideoCapture) -> opencv::Result<Mat> {
//     let mut bad_frame = true;
//     let mut frame = Mat::default()?;
//
//     while bad_frame {
//         capture.read(&mut frame)?;
//         // TODO: Best way to handle this?
//         bad_frame = frame.size()?.width == 0;
//     }
//
//     Ok(frame)
// }

// pub fn record_frames(
//     capture: &mut VideoCapture,
//     num_frames: u16,
//     output_filename: &str,
// ) -> opencv::Result<()> {
//     if num_frames == 0 {
//         return Ok(());
//     }
//
//     let frame = get_frame_opencv(capture)?;
//     let mut video_writer = VideoWriter::new(output_filename, 0, 0.0, frame.size()?, true)?;
//
//     video_writer.write(&frame)?;
//
//     for _ in 0..(num_frames - 1) {
//         video_writer.write(&get_frame_opencv(capture)?)?;
//     }
//
//     video_writer.release()?;
//
//     Ok(())
// }

// pub fn get_alpha_image(path: &str) -> opencv::Result<Mat> {
//     imread(path, IMREAD_UNCHANGED)
// }
