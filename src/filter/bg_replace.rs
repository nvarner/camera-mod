use crate::filter::Filter;
// use crate::get_frames::get_frame_opencv;
// use crate::image_utils::{fill_mask_holes, rgb_to_yuyv};
use std::borrow::Cow;
use std::error::Error;
use crate::frame::Frame;

pub struct BgReplaceFilter {
    // subtractor: Ptr<dyn BackgroundSubtractorMOG2>,
    background_path: String,
    // mode: Mode,
}

impl BgReplaceFilter {
    pub fn new() -> Self {
        // TODO: Make the file location a parameter
        // let mut saved_capture = VideoCapture::from_file("bg_images/img_%02d.png", CAP_IMAGES)?;
        // let subtractor = Self::get_trained_subtractor(&mut saved_capture, 100)?;

        Self {
            // subtractor,
            background_path: "bg/nothing.png".to_owned(),
            // mode: Mode::GaussianBlur,
        }
    }

    // pub fn set_background_path(&mut self, background_path: &str) -> opencv::Result<()> {
    //     self.background_path = background_path.to_owned();
    //     if let Mode::Image(_) = &self.mode {
    //         self.set_image()?;
    //     }
    //     Ok(())
    // }

    pub fn set_blur(&mut self) {
        // self.mode = Mode::GaussianBlur;
    }

    // pub fn set_image(&mut self) -> opencv::Result<()> {
    //     self.mode = Mode::new_image(&self.background_path)?;
    //     Ok(())
    // }

    // fn get_trained_subtractor(
    //     capture: &mut VideoCapture,
    //     num_training_frames: u16,
    // ) -> opencv::Result<Ptr<dyn BackgroundSubtractorMOG2>> {
    //     let mut subtractor =
    //         create_background_subtractor_mog2(num_training_frames as i32, 20.0, false)?;
    //
    //     for _ in 0..num_training_frames {
    //         let frame = get_frame_opencv(capture)?;
    //
    //         let mut color_frame = Mat::default()?;
    //         cvt_color(&frame, &mut color_frame, COLOR_BGR2BGRA, 0).unwrap();
    //
    //         let mut temp = Mat::default()?;
    //         video::BackgroundSubtractor::apply(&mut subtractor, &color_frame, &mut temp, -1.0)?;
    //     }
    //
    //     Ok(subtractor)
    // }

    // fn get_blurred_background(&self, input: &Mat) -> opencv::Result<Mat> {
    //     let mut background = Mat::default()?;
    //     gaussian_blur(
    //         &input,
    //         &mut background,
    //         Size::new(101, 101),
    //         0.0,
    //         0.0,
    //         BORDER_DEFAULT,
    //     )?;
    //     Ok(background)
    // }
}

impl Filter for BgReplaceFilter {
    fn apply_filter(&mut self, mut frame: &mut Frame) -> Result<(), Box<dyn Error>> {
        // TODO: Reimplement
        // let mut color_frame = Mat::default()?;
        // cvt_color(frame, &mut color_frame, COLOR_YUV2BGRA_YUYV, 0)?;
        //
        // let mut fg_mask = Mat::default()?;
        // video::BackgroundSubtractor::apply(&mut self.subtractor, &color_frame, &mut fg_mask, 0.0)?;
        //
        // let mut opened = Mat::default()?;
        // let morphology_shape = MORPH_ELLIPSE;
        // let open_size = 6;
        // let open_kernel = get_structuring_element(
        //     morphology_shape,
        //     Size::new(2 * open_size + 1, 2 * open_size + 1),
        //     Point::new(open_size, open_size),
        // )?;
        // morphology_ex(
        //     &fg_mask,
        //     &mut opened,
        //     MORPH_OPEN,
        //     &open_kernel,
        //     Point::new(-1, -1),
        //     1,
        //     BORDER_CONSTANT,
        //     morphology_default_border_value()?,
        // )?;
        //
        // let mut denoised = Mat::default()?;
        // let close_size = 5;
        // let close_kernel = get_structuring_element(
        //     morphology_shape,
        //     Size::new(2 * close_size + 1, 2 * close_size + 1),
        //     Point::new(close_size, close_size),
        // )?;
        // morphology_ex(
        //     &opened,
        //     &mut denoised,
        //     MORPH_CLOSE,
        //     &close_kernel,
        //     Point::new(-1, -1),
        //     5,
        //     BORDER_CONSTANT,
        //     morphology_default_border_value()?,
        // )?;
        //
        // let no_holes = fill_mask_holes(&denoised)?;
        //
        // let mut final_mask = Mat::default()?;
        // bitwise_not(&no_holes, &mut final_mask, &no_array()?)?;
        //
        // let background = match &self.mode {
        //     Mode::Image(mat) => Cow::Borrowed(mat),
        //     Mode::GaussianBlur => Cow::Owned(self.get_blurred_background(&frame)?),
        // };
        //
        // background.copy_to_masked(&mut frame, &final_mask)?;
        Ok(())
    }
}

// enum Mode {
//     GaussianBlur,
//     Image(Mat),
// }
//
// impl Mode {
//     pub fn new_image(path: &str) -> opencv::Result<Self> {
//         let image = imread(path, IMREAD_COLOR)?;
//         Ok(Self::Image(rgb_to_yuyv(image)?))
//     }
// }
