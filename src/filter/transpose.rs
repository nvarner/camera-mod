use std::error::Error;

use crate::filter::Filter;
use crate::frame::Frame;

pub struct TransposeFilter {}

impl Filter for TransposeFilter {
    fn apply_filter(&mut self, mut frame: &mut Frame) -> Result<(), Box<dyn Error>> {
        // TODO: Reimplement
        // TODO: Account for YUYV to properly transpose the image
        // let transposed = Mat::copy(frame)?;
        // transpose(&transposed, &mut frame)?;
        Ok(())
    }
}
