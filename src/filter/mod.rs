use crate::filter::bg_replace::BgReplaceFilter;
use crate::filter::flip::FlipFilter;
use crate::filter::overlay::OverlayFilter;
use crate::filter::transpose::TransposeFilter;
use std::error::Error;
use crate::frame::Frame;

pub mod bg_replace;
pub mod flip;
pub mod overlay;
pub mod transpose;

pub trait Filter {
    fn apply_filter(&mut self, frame: &mut Frame) -> Result<(), Box<dyn Error>>;
}

pub struct MaybeFilter<T: Filter> {
    filter: T,
    should_apply: bool,
}

impl<T: Filter> MaybeFilter<T> {
    pub fn set_should_apply(&mut self, should_apply: bool) {
        self.should_apply = should_apply;
    }

    pub fn get_filter_mut(&mut self) -> &mut T {
        &mut self.filter
    }

    pub fn replace_filter(&mut self, new_filter: T) {
        self.filter = new_filter;
    }
}

impl<T: Filter> From<T> for MaybeFilter<T> {
    fn from(filter: T) -> Self {
        Self {
            filter,
            should_apply: false,
        }
    }
}

impl<T: Filter> Filter for MaybeFilter<T> {
    fn apply_filter(&mut self, frame: &mut Frame) -> Result<(), Box<dyn Error>> {
        if self.should_apply {
            self.filter.apply_filter(frame)
        } else {
            Ok(())
        }
    }
}

pub struct Filters {
    pub bg_replace: MaybeFilter<BgReplaceFilter>,
    pub flip: MaybeFilter<FlipFilter>,
    pub transpose: MaybeFilter<TransposeFilter>,
    pub overlay: MaybeFilter<OverlayFilter>,
}

impl Filters {
    pub fn new(
        bg_replace: BgReplaceFilter,
        flip: FlipFilter,
        transpose: TransposeFilter,
        overlay: OverlayFilter,
    ) -> Self {
        Self {
            bg_replace: bg_replace.into(),
            flip: flip.into(),
            transpose: transpose.into(),
            overlay: overlay.into(),
        }
    }
}

macro_rules! apply_filters_from_self {
    ( $self:ident, $mat_id:ident, $( $filter_id:ident ),* ) => {
        $(
            $self.$filter_id.apply_filter($mat_id)?;
        )*
    };
}

impl Filter for Filters {
    fn apply_filter(&mut self, frame: &mut Frame) -> Result<(), Box<dyn Error>> {
        apply_filters_from_self!(self, frame, bg_replace, flip, transpose, overlay);
        Ok(())
    }
}
