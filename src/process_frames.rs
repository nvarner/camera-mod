use crate::camera::get_camera;
use crate::comms::{Command, Response};
use crate::filter::bg_replace::BgReplaceFilter;
use crate::filter::flip::FlipFilter;
use crate::filter::overlay::OverlayFilter;
use crate::filter::transpose::TransposeFilter;
use crate::filter::{Filter, Filters};
use crate::get_frames::get_frame;
use crate::v4l2loopback::V4l2Loopback;
use crate::{HEIGHT, WIDTH};
#[cfg(feature = "profile")]
use std::fs::File;
use std::io;
use std::sync::mpsc::{Receiver, SendError, Sender, TryRecvError};
use thiserror::Error;
use v4l::prelude::UserptrStream;
use crate::frame::Frame;
use v4l::buffer::Type;

struct State {
    pub running: bool,
    pub should_select_background: bool,
    pub should_flip: bool,
    pub should_transpose: bool,
    pub should_overlay: bool,
}

impl Default for State {
    fn default() -> Self {
        Self {
            running: true,
            should_select_background: false,
            should_flip: false,
            should_transpose: false,
            should_overlay: false,
        }
    }
}

pub fn process_frames(tx: &Sender<Response>, rx: &Receiver<Command>) -> Result<(), BgReplaceError> {
    let cam = get_camera(WIDTH, HEIGHT).map_err(BgReplaceError::CreateCameraStream)?;
    let mut cam_stream =
        UserptrStream::with_buffers(&cam, Type::VideoCapture, 2).map_err(BgReplaceError::CreateCameraStream)?;

    let mut loopback =
        V4l2Loopback::new("/dev/video2", WIDTH, HEIGHT).map_err(BgReplaceError::ConnectLoopback)?;

    println!("Learning background...");
    let bg_replace_filter = BgReplaceFilter::new();
    println!("Trained!");
    let flip_filter = FlipFilter {};
    let transpose_filter = TransposeFilter {};
    let overlay_filter = OverlayFilter::new("overlay/check.png");
    let mut filters = Filters::new(
        bg_replace_filter,
        flip_filter,
        transpose_filter,
        overlay_filter,
    );

    tx.send(Response::Initialized)?;

    #[cfg(feature = "profile")]
    let guard = pprof::ProfilerGuard::new(100000).unwrap();

    while handle_commands(&mut filters, &rx)? {
        println!("test");
        // Sometimes, the buffers fail for unknown reasons. When that happens, just recreate them.
        let mut frame = match get_frame(&mut cam_stream, WIDTH, HEIGHT) {
            Ok(frame) => frame,
            Err(_e) => {
                drop(cam_stream);
                cam_stream = UserptrStream::with_buffers(&cam, Type::VideoCapture, 2)
                    .map_err(BgReplaceError::CreateCameraStream)?;
                println!("New stream");
                continue;
            }
        };

        println!("Got frame");
        process_frame(&mut frame, &mut filters, &mut loopback)?;
        println!("Loop end");
    }

    #[cfg(feature = "profile")]
    if let Ok(report) = guard.report().build() {
        // Flamegraph
        let file = File::create("flamegraph.svg").unwrap();
        report.flamegraph(file).unwrap();

        // Stack traces
        println!("report: {}", &report);
    }

    println!("Thread exited normally");

    Ok(())
}

fn process_frame(
    frame: &mut Frame,
    filters: &mut Filters,
    loopback: &mut V4l2Loopback,
) -> Result<(), BgReplaceError> {
    filters
        .apply_filter(frame)
        .map_err(|err| BgReplaceError::Other(err.to_string()))?;

    // TODO: Create custom error
    loopback
        .write_frame(&frame)
        .map_err(|err| BgReplaceError::Other(err.to_string()))?;

    Ok(())
}

fn handle_commands(filters: &mut Filters, rx: &Receiver<Command>) -> Result<bool, BgReplaceError> {
    loop {
        match rx.try_recv() {
            Ok(command) => match command {
                Command::ShouldFlip(should_flip) => {
                    filters.flip.set_should_apply(should_flip);
                }
                Command::ShouldTranspose(should_transpose) => {
                    filters.transpose.set_should_apply(should_transpose);
                }
                Command::ShouldSelectBackground(should_select) => {
                    filters.bg_replace.set_should_apply(should_select);
                }
                Command::SetBackgroundPath(background_image) => {
                    // filters
                    //     .bg_replace
                    //     .get_filter_mut()
                    //     .set_background_path(&background_image)?;
                }
                Command::SetBackgroundBlur => {
                    filters.bg_replace.get_filter_mut().set_blur();
                }
                Command::SetBackgroundImage => {
                    // filters.bg_replace.get_filter_mut().set_image()?;
                }
                Command::ShouldOverlay(should_overlay) => {
                    filters.overlay.set_should_apply(should_overlay);
                }
                Command::SetOverlay(overlay_image_path) => {
                    filters
                        .overlay
                        .replace_filter(OverlayFilter::new(&overlay_image_path));
                }
                Command::Stop => {
                    return Ok(false);
                }
            },
            Err(error) => match error {
                TryRecvError::Empty => break,
                TryRecvError::Disconnected => return Err(BgReplaceError::ReceiveFromThread),
            },
        }
    }
    Ok(true)
}

#[derive(Error, Debug)]
pub enum BgReplaceError {
    #[error("Could not create camera stream")]
    CreateCameraStream(#[source] io::Error),
    #[error("Could not connect to the loopback device")]
    ConnectLoopback(#[source] io::Error),
    // #[error("Error from OpenCV")]
    // OpenCV(#[from] opencv::Error),
    #[error("Could not send to the thread because it stopped")]
    SendToThread(#[from] SendError<Response>),
    #[error("Could not receive from the thread because it stopped")]
    ReceiveFromThread,
    #[error("There was an error: {0}")]
    Other(String),
}
