use std::fmt;
use std::error::Error;

/// Stores a single YUYV encoded frame of image data
pub struct Frame {
    data: Vec<u8>,
    /// Width of the frame; must be even because of the YUYV encoding
    width: usize,
    height: usize,
}

impl Frame {
    pub fn new(data: &[u8], width: usize, height: usize) -> Result<Self, SizeMismatchError> {
        let expected_data_size = width * height * 2;
        if data.len() != expected_data_size {
            Err(SizeMismatchError::new(data.len(), expected_data_size))
        } else {
            Ok(Self {
                data: data.into(),
                width,
                height
            })
        }
    }

    pub fn get_data(&self) -> &[u8] {
        self.data.as_slice()
    }
}

#[derive(Debug)]
pub struct SizeMismatchError {
    data_size: usize,
    frame_size: usize,
}

impl SizeMismatchError {
    pub fn new(data_size: usize, frame_size: usize) -> Self {
        Self {
            data_size,
            frame_size,
        }
    }
}

impl fmt::Display for SizeMismatchError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(
            f,
            "Mismatch between data size and frame size; got {} bytes but expected {} bytes",
            self.data_size, self.frame_size
        )
    }
}

impl Error for SizeMismatchError {}
