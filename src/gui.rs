use crate::comms::{Command, Response};
use gtk::prelude::*;
use std::sync::mpsc::{Receiver, Sender};

pub struct MainWindow {
    window: gtk::Window,
    flip_button: gtk::CheckButton,
    transpose_button: gtk::CheckButton,
    select_bg_button: gtk::CheckButton,
    bg_mode_menu: gtk::ComboBoxText,
    bg_image_select_button: gtk::FileChooserButton,
    overlay_button: gtk::CheckButton,
    overlay_select_button: gtk::FileChooserButton,
}

impl MainWindow {
    pub fn new() -> Self {
        let source = include_str!("gui-layout.glade");
        let builder = gtk::Builder::from_string(source);

        let window = builder.get_object("main-window").unwrap();
        let flip_button = builder.get_object("flip-button").unwrap();
        let transpose_button = builder.get_object("transpose-button").unwrap();
        let select_bg_button = builder.get_object("select-background-button").unwrap();
        let bg_mode_menu = builder.get_object("background-mode-menu").unwrap();
        let bg_image_select_button = builder
            .get_object("background-image-select-button")
            .unwrap();
        let overlay_button = builder.get_object("overlay-button").unwrap();
        let overlay_select_button = builder.get_object("overlay-select-button").unwrap();

        Self {
            window,
            flip_button,
            transpose_button,
            select_bg_button,
            bg_mode_menu,
            bg_image_select_button,
            overlay_button,
            overlay_select_button,
        }
    }

    pub fn start(&self, tx: Sender<Command>, _rx: &Receiver<Response>) {
        let flip_tx = tx.clone();
        self.flip_button.connect_clicked(move |button| {
            flip_tx
                .send(Command::ShouldFlip(button.get_active()))
                .expect("Couldn't communicate with thread");
        });

        let transpose_tx = tx.clone();
        self.transpose_button.connect_clicked(move |button| {
            transpose_tx
                .send(Command::ShouldTranspose(button.get_active()))
                .expect("Couldn't communicate with thread");
        });

        let select_bg_tx = tx.clone();
        self.select_bg_button.connect_clicked(move |button| {
            select_bg_tx
                .send(Command::ShouldSelectBackground(button.get_active()))
                .expect("Couldn't communicate with thread");
        });

        let bg_mode_tx = tx.clone();
        self.bg_mode_menu.connect_changed(move |menu| {
            let command = match menu.get_active_id().unwrap().as_str() {
                "gaussian-blur" => Command::SetBackgroundBlur,
                "image" => Command::SetBackgroundImage,
                _ => panic!("Unknown background type"),
            };

            bg_mode_tx
                .send(command)
                .expect("Couldn't communicate with thread");
        });

        let bg_image_select_tx = tx.clone();
        self.bg_image_select_button
            .connect_selection_changed(move |button| {
                let file = button
                    .get_uri()
                    .unwrap()
                    .chars()
                    .skip("file://".len())
                    .collect();
                bg_image_select_tx
                    .send(Command::SetBackgroundPath(file))
                    .expect("Couldn't communicate with thread");
            });

        let overlay_button_tx = tx.clone();
        self.overlay_button.connect_clicked(move |button| {
            overlay_button_tx
                .send(Command::ShouldOverlay(button.get_active()))
                .expect("Couldn't communicate with thread");
        });

        let overlay_select_tx = tx;
        self.overlay_select_button
            .connect_selection_changed(move |button| {
                let file = button
                    .get_uri()
                    .unwrap()
                    .chars()
                    .skip("file://".len())
                    .collect();
                overlay_select_tx
                    .send(Command::SetOverlay(file))
                    .expect("Couldn't communicate with thread");
            });

        self.window.set_wmclass("Camera Mod", "Camera Mod");
        self.window.connect_delete_event(|_, _| {
            gtk::main_quit();
            Inhibit(false)
        });
        self.window.show_all();
        gtk::main();
    }
}
