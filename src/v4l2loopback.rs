// Partially based on https://github.com/floe/deepbacksub

use std::error::Error;

use std::io;
use std::io::{Write, BufWriter};
use std::slice::from_raw_parts;
use v4l::format::{Colorspace, FieldOrder, Format};
use v4l::prelude::*;
use v4l::FourCC;
use crate::frame::Frame;
use v4l::video::Output;

pub struct V4l2Loopback {
    pub device: BufWriter<Device>,
    width: usize,
    height: usize,
}

impl V4l2Loopback {
    pub fn new(v4l2_device_path: &str, width: usize, height: usize) -> io::Result<Self> {
        let line_width = 2 * width as u32;
        let frame_size = height as u32 * line_width;

        let mut device = Device::with_path(v4l2_device_path)?;

        let mut format = Format::new(width as u32, height as u32, FourCC::new(b"YUYV"));
        format.size = frame_size;
        format.field_order = FieldOrder::Progressive;
        format.stride = line_width;
        format.colorspace = Colorspace::SRGB;
        device.set_format(&format)?;
        println!("Format: {}", device.format()?);

        let device = BufWriter::new(device);

        Ok(Self {
            device,
            width,
            height,
        })
    }

    pub fn write_frame(&mut self, frame: &Frame) -> io::Result<()> {
        // TODO: Resize when recording images to process less if a lower resolution is desired
        // TODO: Reimplement resize?

        self.device.write_all(frame.get_data())?;
        Ok(())
    }
}
