use std::slice;

const YUYV_BYTES_PER_PIXEL: usize = 2;

// pub fn fill_mask_holes(mask: &Mat) -> opencv::Result<Mat> {
//     let mut connected_mask = mask.clone();
//     connect_bottom_mask(&mut connected_mask)?;
//
//     let mut contours: Vector<Vector<Point>> = Vector::new();
//     let mut hierarchy = Mat::default()?;
//     find_contours_with_hierarchy(
//         &connected_mask,
//         &mut contours,
//         &mut hierarchy,
//         RETR_LIST,
//         CHAIN_APPROX_NONE,
//         Point::default(),
//     )?;
//
//     // Get the right size, but different color scheme and filled black
//     let mut contour_result = Mat::copy(mask)?;
//
//     let color = Scalar::new(255.0, 255.0, 255.0, 255.0);
//     for i in 0i32..contours.len() as i32 {
//         draw_contours(
//             &mut contour_result,
//             &contours,
//             i,
//             color,
//             -1,
//             LINE_8,
//             &no_array()?,
//             0,
//             Point::default(),
//         )?;
//     }
//
//     Ok(contour_result)
// }

// pub fn flip_yuyv_frame(mut frame: &mut Mat) -> opencv::Result<()> {
//     let cols = frame.cols() as usize;
//     let floor_half_cols = cols / 2;
//     let rows = frame.rows() as usize;
//     let data: &mut [u8] = raw_data(&mut frame, YUYV_BYTES_PER_PIXEL)?;
//
//     // Flip each row horizontally
//     // In YUYV, each pair of pixels shares some data and each pixel has its own data
//     // As a result, opposing pairs of pixels need to be flipped together
//     for row in 0..rows {
//         let row_start = row * cols;
//         for left_px_col_1 in (0..floor_half_cols).step_by(2) {
//             let right_px_col_2 = cols - left_px_col_1 - 1;
//
//             // px1,px2 = Y1 U, Y2 V
//             // ...,lpx1,lpx2,...,rpx1,rpx2,... -> ...,rpx2,rpx1,...,lpx2,lpx1,...
//
//             let ly1_index = YUYV_BYTES_PER_PIXEL * (left_px_col_1 + row_start);
//             let lu_index = ly1_index + 1;
//             let ly2_index = ly1_index + 2;
//             let lv_index = ly1_index + 3;
//
//             let ry2_index = YUYV_BYTES_PER_PIXEL * (right_px_col_2 + row_start);
//             let ry1_index = ry2_index - 2;
//             let ru_index = ry2_index - 1;
//             let rv_index = ry2_index + 1;
//
//             let ly1 = data[ly1_index];
//             let lu = data[lu_index];
//             let ly2 = data[ly2_index];
//             let lv = data[lv_index];
//
//             data[ly1_index] = data[ry2_index];
//             data[lu_index] = data[ru_index];
//             data[ly2_index] = data[ry1_index];
//             data[lv_index] = data[rv_index];
//
//             data[ry2_index] = ly1;
//             data[ru_index] = lu;
//             data[ry1_index] = ly2;
//             data[rv_index] = lv;
//         }
//     }
//
//     Ok(())
// }

// // From https://github.com/floe/deepbacksub/blob/master/deepseg.cc
// pub fn rgb_to_yuyv(frame: Mat) -> opencv::Result<Mat> {
//     let mut tmp = Mat::default()?;
//     cvt_color(&frame, &mut tmp, COLOR_RGB2YUV, 0)?;
//     let mut yuv = VectorOfMat::new();
//     split(&tmp, &mut yuv)?;
//
//     let mut y_owned = yuv.get(0)?;
//     let mut u_owned = yuv.get(1)?;
//     let mut v_owned = yuv.get(2)?;
//
//     let y_data: &mut [u8] = raw_data(&mut y_owned, 1)?;
//     let u_data: &mut [u8] = raw_data(&mut u_owned, 1)?;
//     let v_data: &mut [u8] = raw_data(&mut v_owned, 1)?;
//
//     unsafe {
//         let mut yuyv = Mat::new_rows_cols(tmp.rows(), tmp.cols(), CV_8UC2)?;
//         let pixel_count = yuyv.total()?;
//         let out_data: &mut [u8] = raw_data(&mut yuyv, YUYV_BYTES_PER_PIXEL)?;
//
//         for i in (0..pixel_count).step_by(2) {
//             let u = (((u_data[i] as u16) + (u_data[i + 1] as u16)) / 2) as u8;
//             let v = (((v_data[i] as u16) + (v_data[i + 1] as u16)) / 2) as u8;
//
//             let base = YUYV_BYTES_PER_PIXEL * i;
//             out_data[base] = y_data[i];
//             out_data[base + 1] = v;
//             out_data[base + 2] = y_data[i + 1];
//             out_data[base + 3] = u;
//         }
//
//         Ok(yuyv)
//     }
// }

// // From https://stackoverflow.com/a/55695349/7432915
// pub fn overlay_transparent(mat: &mut Mat, overlay: &Mat) -> opencv::Result<()> {
//     // Split the overlay
//     let mut layers = VectorOfMat::with_capacity(4);
//     split(overlay, &mut layers)?;
//
//     // Take the RGB layers and make them into their own `Mat`
//     let no_trans_overlay = VectorOfMat::from_iter(layers.iter().take(3));
//     let mut no_trans_overlay_mat = Mat::default()?;
//     merge(&no_trans_overlay, &mut no_trans_overlay_mat)?;
//
//     // Make that `Mat` YUYV
//     let color_overlay = rgb_to_yuyv(no_trans_overlay_mat)?;
//
//     // Copy it onto the frame using the alpha channel as the mask
//     color_overlay.copy_to_masked(mat, &layers.get(3)?)?;
//
//     Ok(())
// }

// fn raw_data(mat: &mut Mat, bytes_per_pixel: usize) -> opencv::Result<&mut [u8]> {
//     let pixel_count = mat.total()?;
//
//     // mat.data_typed_unchecked_mut is implemented wrong. This is the correct implementation.
//     unsafe {
//         Ok(slice::from_raw_parts_mut(
//             mat.data_mut() as *mut _ as *mut _,
//             pixel_count * bytes_per_pixel,
//         ))
//     }
// }

// fn connect_bottom_mask(mut mat: &mut Mat) -> opencv::Result<()> {
//     let last_row_index = mat.rows() - 1;
//
//     let mut last_row = mat.row(last_row_index)?;
//     let data = raw_data(&mut last_row, 1)?;
//
//     let leftmost_white =
//         data.iter()
//             .enumerate()
//             .find_map(|(col, pixel)| if *pixel == 255 { Some(col) } else { None });
//     let leftmost_white = match leftmost_white {
//         Some(col) => col,
//         None => return Ok(()),
//     };
//     // If we haven't returned above, we must find some rightmost white pixel
//     let rightmost_white = data
//         .iter()
//         .enumerate()
//         .rev()
//         .find_map(|(col, pixel)| if *pixel == 255 { Some(col) } else { None })
//         .unwrap();
//
//     line(
//         &mut mat,
//         Point::new(leftmost_white as i32, last_row_index),
//         Point::new(rightmost_white as i32, last_row_index),
//         Scalar::all(255.0),
//         1,
//         LINE_8,
//         0,
//     )?;
//
//     Ok(())
// }
