use crate::process_frames::BgReplaceError;

pub enum Command {
    ShouldFlip(bool),
    ShouldTranspose(bool),
    ShouldSelectBackground(bool),
    SetBackgroundImage,
    SetBackgroundBlur,
    SetBackgroundPath(String),
    ShouldOverlay(bool),
    SetOverlay(String),
    Stop,
}

pub enum Response {
    Initialized,
    Error(Box<BgReplaceError>),
}
