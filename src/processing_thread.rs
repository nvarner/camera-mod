use crate::comms::{Command, Response};
use crate::process_frames::process_frames;
use std::error::Error;
use std::sync::mpsc::{Receiver, Sender};
use std::thread;
use std::thread::JoinHandle;

pub fn start(
    thread_rx: Receiver<Command>,
    thread_tx: Sender<Response>,
    main_rx: &Receiver<Response>,
) -> Result<JoinHandle<()>, Box<dyn Error>> {
    let handle = thread::spawn(move || {
        run_thread(thread_rx, thread_tx);
    });
    wait_for_init(&main_rx)?;
    Ok(handle)
}

fn run_thread(thread_rx: Receiver<Command>, thread_tx: Sender<Response>) {
    let bg_sub_result = process_frames(&thread_tx, &thread_rx);
    if let Err(err) = bg_sub_result {
        thread_tx
            .send(Response::Error(Box::new(err)))
            .expect("Could not communicate error to main thread, crashing");
    }
}

fn wait_for_init(main_rx: &Receiver<Response>) -> Result<(), Box<dyn Error>> {
    match main_rx.recv() {
        Ok(response) => match response {
            Response::Initialized => Ok(()), // Expected
            Response::Error(err) => {
                // TODO: Improve error handling
                eprintln!("There was an error initializing the thread");
                Err(Box::new(err))
            }
        },
        Err(error) => {
            // TODO: Improve error handling
            eprintln!("The thread crashed before it initialized");
            Err(Box::new(error))
        }
    }
}
